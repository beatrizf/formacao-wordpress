# Retratos-Eternos---Desenvolvedora-Beatriz

Projeto final da Formação WordPress EJCM

Plugins: Elementor, Duplicator, VVAme Chat

Tema: Raft

A Retratos Eternos é uma empresa de fotografia que realiza sessões de fotos para profissionais que querem melhorar seu posicionamento no mercado e para famílias que querem eternizar os momentos que passam juntos.
